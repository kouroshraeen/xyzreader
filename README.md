# Make Your App Material

Make Your App Material is the fifth student project for the Udacity's [Android Developer Nanodegree by Google](https://www.udacity.com/course/android-developer-nanodegree-by-google--nd801).

## Screenshots
| List                     | Detail                    |
| ------------------------ |:-------------------------:|
|![screen](./art/list.png) |![screen](./art/detail.png)|

## Additions and Changes

* Updated the look and feel of an Android app to meet Material Design specifications.
* Added CoordinatorLayout, AppBarLayout, and CollapsingToolbarLayout.
* Replaced ActionBar with ToolBar.
* Implemented shared elements transition.
* Defined a consistent color theme for the app.
* Ensured the app provides sufficient space between text and surrounding elements.
* Replaced ImageButton with FloatingActionButton.
